import {StyleSheet} from 'react-native';

export default StyleSheet.create ({
  registerPageContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#1C1D34',
    paddingTop: 10,
  },
  titleText: {
    fontSize: 25,
    marginLeft: 10,
    color: '#ffff',
    fontWeight: 'bold',
  },
  form: {
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});
