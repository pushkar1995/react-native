import React, { Component } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import styles from './RegistrationScreenStyles'
import RegistrationForm from './RegistrationForm'
import RegistrationActions from '../../../Redux/RegistrationRedux'
import { NavigationActions } from 'react-navigation';
class RegistrationScreen extends Component {
  render() {
    if(this.props.userData.success === 1){
      this.props.navigation.dispatch(NavigationActions.back())
      this.props.clearRegistrationForm()
    }
    // const errrorMsg = this.props.userData.error. ? this.props.userData.error : null
    const errorText = this.props.userData.error ? 'hello' : 'hi'
    console.log(this.props.userData)
    return (
      <ScrollView style={styles.registerPageContainer}>
        <Text style={styles.titleText}>Sign up</Text>
        <View style={styles.form}>
          <RegistrationForm
            type="Submit"
            onSubmit={this.props.onAddNewUser}
            errorText={errorText}
          />
        </View>
      </ScrollView>
    )
  }
}
const maStateToProps = state => ({
  userData: state.registration
})
const mapDispatchToProps = dispatch => ({
  onAddNewUser: data => {
    dispatch(RegistrationActions.userRequest(data))
  },
  clearRegistrationForm:()=>{
    dispatch(RegistrationActions.clearRegistrationForm())
  }
})

export default connect(
  maStateToProps,
  mapDispatchToProps
)(RegistrationScreen)
