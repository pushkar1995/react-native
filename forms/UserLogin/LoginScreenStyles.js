import {StyleSheet} from 'react-native';

export default StyleSheet.create ({
  loginPageContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#1C1E33',
  },
  titleText: {
    fontSize: 25,
    color: '#ffff',
    fontWeight: 'bold',
    justifyContent: 'flex-start',
    marginLeft: 10,
  },
  form: {
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  orText: {
    color: '#ffff',
    fontSize: 16,
    paddingTop: 15,
  },
  socialIcon: {
    flex: 2,
    flexDirection: 'column',
  },
  forgetText: {
    fontSize: 16,
    fontWeight: '100',
    color: '#078AC0',
    textAlign: 'center',
  },
});
