import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';

import styles from './LoginScreenStyles';

import LoginForm from './LoginForm';
import FacebookIcon from '../../SocialIcons/FacebookIcon';
import ForgetPassword from '../ForgetPassword/ForgetPassword';
import LoginActionCreators from '../../../Redux/LoginRedux';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';

class LoginScreen extends Component {
  constructor () {
    super ();
    this.state = {
      showModal: false,
    };
    this.hideModal = this.hideModal.bind (this);
  }
  componentWillReceiveProps (nextProps) {
    // console.log ('Eloooooo', nextProps.facebookLogin);
    if (nextProps.facebookLogin.success || this.props.success) {
      this.props.navigation.navigate ('LaunchScreen');
    }
  }
  hideModal () {
    this.setState ({showModal: false});
  }

  render () {
    return (
      <ScrollView style={styles.loginPageContainer}>
        <Text style={styles.titleText}>Sign In</Text>

        <View style={styles.form}>
          <LoginForm type="Sign In" onSubmit={this.props.onUserLogin} />

          <Text style={styles.orText}>Or</Text>
        </View>

        <View style={styles.socialIcon}>
          <FacebookIcon />
        </View>

        <TouchableOpacity>
          <ForgetPassword
            showModal={this.state.showModal}
            hideModal={this.hideModal}
          />
          <Text
            style={styles.forgetText}
            onPress={() => this.setState ({showModal: true})}
            //onPress={() => this.props.navigation.navigate ('ForgetPasswordPage')}
          >
            Forget Password?
          </Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

mapStateToProps = state => {
  console.log ('state has changed', state);
  return {
    success: state.login.success,
    facebookLogin: state.facebookLogin,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onUserLogin: data => {
      dispatch (LoginActionCreators.loginSubmitRequest (data));
    },
  };
};

export default connect (mapStateToProps, mapDispatchToProps) (LoginScreen);
