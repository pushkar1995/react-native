import {StyleSheet} from 'react-native';

export default StyleSheet.create ({
  rootContainer: {
    flex: 1,
    backgroundColor: '#1C1E33',
  },
  iconContainer: {
    justifyContent: 'flex-end',
    alignSelf: 'flex-end',
  },
  crossIcon: {
    alignSelf: 'flex-end',
    color: '#ffff',
  },
  headerContent: {
    marginHorizontal: 30,
  },
  passwordResetTitle: {
    fontSize: 20,
    color: '#ffff',
    paddingBottom: 5,
  },
  instructionText: {
    color: '#ffff',
  },
  bodyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
  },
  inputBox: {
    width: 350,
    height: 60,
    borderBottomWidth: 1,
    borderColor: '#078AC0',
    fontSize: 15,
    color: 'white',
    fontWeight: '100',
    textAlign: 'right',
  },
  sendButton: {
    justifyContent: 'center',
    //alignSelf: 'right',
    width: 100,
    height: 40,
    borderColor: '#02A2DF',
    borderWidth: 0.5,
    borderRadius: 100,
    marginTop: 20,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '100',
    color: '#078AC0',
    textAlign: 'center',
  },
});
