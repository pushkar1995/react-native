import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  Text,
  StyleSheet,
  Modal,
} from 'react-native';
import CrossIcon from 'react-native-vector-icons/Entypo';

import styles from './ForgetPasswordStyles';

import {connect} from 'react-redux';
import ForgetPasswordActionCreators from '../../../Redux/ForgetPasswordRedux';

class ForgetPasswordPage extends Component {
  constructor (props) {
    super (props);
    this.state = {
      email: '',
    };
  }

  updateValue (text, field) {
    if (field == 'email') {
      this.setState ({
        email: text,
      });
    }
  }

  render () {
    return (
      <Modal
        visible={this.props.showModal}
        transparent={true}
        animationType="fade"
      >
        <View style={styles.rootContainer}>
          <View style={styles.iconContainer}>
            <TouchableOpacity
              onPress={() => {
                this.props.hideModal ();
              }}
            >
              <CrossIcon
                name="circle-with-cross"
                style={styles.crossIcon}
                size={20}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.headerContent}>
            <Text style={styles.passwordResetTitle}>Password reset</Text>
            <Text style={styles.instructionText}>
              Instructions to reset the password will be sent to the provided email address
            </Text>
          </View>

          <View style={styles.bodyContainer}>

            <TextInput
              style={styles.inputBox}
              placeholder="Email"
              placeholderColor="#90caf9"
              placeholderTextColor="#D2D2D2"
              selectionColor="#fff"
              underlineColorAndroid="transparent"
              onChangeText={text => this.updateValue (text, 'email')}
              //autoFocus
            />
            <TouchableOpacity
              style={styles.sendButton}
              onPress={this.props.onNewPasswordRequest (this.state)}
            >
              <Text style={styles.buttonText}>Send</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onNewPasswordRequest: data => {
      dispatch (ForgetPasswordActionCreators.newPasswordRequest (data));
    },
  };
};
export default connect (null, mapDispatchToProps) (ForgetPasswordPage);
