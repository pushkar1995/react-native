import React, {Component} from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity  } from 'react-native';

//Styles
import styles from './SearchBoxStyles';

import SearchIcon from 'react-native-vector-icons/EvilIcons';

export default class SearchBox extends Component {
    constructor(props) {
        super(props)

        this.state = {
            SearchB : ''
        }
        this._Search = this._Search.bind(this)
    }

    _Search() {
        
    }
    
  render() {
    return (
      <View style={styles.container}>

        <TextInput style = {styles.inputBox}
            id = 'SearchB'
            placeholder ='Search here'
            placeholderColor =  '#90caf9'
            placeholderTextColor = '#ffff' 
            selectionColor = '#fff'
            onChangeText = {(searchB) => this.setState({SearchB: searchB})}
            value = {this.state.SearchB}
            underlineColorAndroid="transparent"
            //autoFocus
        />

        <View style={styles.searchIcon}>
            <TouchableOpacity onPress={this._Search}>
            <SearchIcon name="search" size={30} color= "#D8D9DE"/>
            </TouchableOpacity>
        </View>

      </View>
    );
  }
}

