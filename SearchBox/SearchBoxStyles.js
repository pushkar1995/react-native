import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        marginLeft: 30,
        marginTop: 15
      },
    
      searchIcon: {
        position: 'absolute',
        marginLeft:310,   
        marginVertical:12,
      },
    
      inputBox: {
        width: 350,
        height: 50,
        backgroundColor:'#2C2E4D',
        borderRadius: 25,
        paddingHorizontal: 20,
        fontSize: 12,
        color: '#ffff',
      },

})