import React, {Component} from 'react';
import {Text, View, TextInput, TouchableOpacity, Keyboard} from 'react-native';

import styles from './LoginFormStyles';

export default class LoginForm extends Component {
  constructor (props) {
    super (props);

    this.state = {
      email: '',
      email_error: '',
      password: '',
      password_error: '',
      error: false,
    };

    this.handleChange = this.handleChange.bind (this);
  }
  handleChange(name, value){
    let state = this.state
    state[name] = value
    this.setState({
      ...state
    })
  }
  validateData(){
    let user = this.state
    user.error = false
    user.email_error = ""
    user.password_error = ""

  if(user.email === ""){
    user.email_error = "Email is required."
    user.error = true
  }
  else{
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const valid_email = re.test(String(user.email).toLowerCase());
    if (!valid_email){
      user.email_error = "Valid Email is required."
      user.error = true
    }
  }
  if (user.password === ""){
    user.password_error = "Password is required."
    user.error = true
  }
  else{
    if (user.password.length < 6){
      user.password_error = "Password must be six characters or more."
      user.error = true
    }
  }
  return user
  }
  handleSubmit = (e) => {
    e.preventDefault();
    const user = this.validateData()
    if (user.error){
      this.setState({
        ...user
      })
    }
    else{
      this.props.handleSubmit(this.state);
    }
  }

  render () {
    return (
      <View style={styles.container}>

        <TextInput
          style={styles.inputBox}
          type="text"
          name="email"
          placeholder="Email"
          placeholderColor="#90caf9"
          placeholderTextColor="#ffff"
          selectionColor="#fff"
          handleChange={this.handleChange}
          value={this.state.email}
          underlineColorAndroid="transparent"
          autoFocus
        />

        <TextInput
          style={styles.inputBox}
          name="password"
          placeholder="Password"
          secureTextEntry={true}
          placeholderColor="#90caf9"
          placeholderTextColor="#ffffff"
          value={this.state.password}
          handleChange={this.handleChange}
          underlineColorAndroid="transparent"
        />
        <Text style={{color: 'red', textAlign: 'right'}}>
          {this.state.Error}
        </Text>

        <TouchableOpacity style={styles.loginButton} onPress={this.handleSubmit}>
          <Text style={styles.buttonText}>{this.props.type}</Text>
        </TouchableOpacity>

      </View>
    );
  }
}
