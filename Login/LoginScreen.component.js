import React, {Component} from 'react';
import {View, Text, ScrollView} from 'react-native';

import styles from './LoginScreenStyles';
import LoginForm from './LoginForm';
import SocialIcons from '../../SocialIcons/SocialIcons';

export default class LoginScreen extends Component {
  render () {
    return (
      <ScrollView style={styles.container}>

        <Text style={styles.titleText}>Sign In</Text>

        <View style={styles.form}>
          <LoginForm type="Sign In" />

          <Text style={styles.orText}>
            Or
          </Text>
        </View>

        <View style={SocialIcons.styles}>
          <SocialIcons />
        </View>

      </ScrollView>
    );
  }
}
