import {StyleSheet} from 'react-native';

export default StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#1C1D34',
    paddingHorizontal: 25,
  },
  titleText: {
    fontSize: 25,
    color: '#ffff',
    fontWeight: 'bold',
  },
  form: {
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  orText: {
    color: '#ffff',
    fontSize: 16,
    paddingTop: 15,
  },
  socialIcon: {
    flex: 2,
    flexDirection: 'column',
  },
});

