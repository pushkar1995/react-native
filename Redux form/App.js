/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

// import LoginPage from './src/components/forms/UserLogin/LoginPage.component';
import RegistrationPage
  from './src/components/forms/UserRegistration/RegistrationPage.component';

export default class App extends Component {
  render () {
    return (
      <View style={styles.container}>
        <RegistrationPage />

      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1C1E33',
  },
});
