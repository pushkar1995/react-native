import React, {Component} from 'react';
import {Text, View, ScrollView} from 'react-native';

import styles from './LoginPageStyles';

import LoginForm from './LoginForm';
import SocialIcons from '../../SocialIcons/SocialIcons';

export default class LoginPage extends Component {
  render () {
    return (
      <ScrollView style={styles.loginPageContainer}>

        <Text style={styles.titleText}>Sign In</Text>

        <View style={styles.form}>
          <LoginForm />

          <Text style={styles.orText}>
            Or
          </Text>
        </View>

        <View style={styles.socialIcon}>
          <SocialIcons />
        </View>

      </ScrollView>
    );
  }
}
