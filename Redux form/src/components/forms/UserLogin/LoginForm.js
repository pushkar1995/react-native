import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';

import styles from './LoginFormStyles';

//define validation
const validate = values => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Email is required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test (values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.password) {
    errors.password = 'Password is required';
  } else if (values.password.length < 8) {
    errors.password = 'Password must be minimum 8 characters';
  } else if (!/^[a-zA-Z0-9 ]$/i.test (values.password)) {
    errors.password = 'password must be alphanumeric';
  }

  return errors;
};

const submit = values => {
  alert (values);
};

// render your fields
const myFields = ({
  label,
  secureTextEntry,
  type,
  meta: {error, touched},
  input: {onChange},
}) => {
  return (
    <View>
      <TextInput
        style={styles.inputBox}
        placeholder={label}
        type={type}
        secureTextEntry={secureTextEntry}
        onChangeText={onChange}
        placeholderTextColor="#ffff"
        underlineColorAndroid="transparent"
      />
      {touched && (error && <Text style={{color: 'red'}}>{error}</Text>)}
    </View>
  );
};

const LoginForm = props => {
  const {handleSubmit} = props;
  return (
    <View>
      <Field
        style={styles.inputField}
        name="email"
        component={myFields}
        label="E-mail"
        type="email"
      />

      <Field
        style={styles.inputField}
        name="password"
        secureTextEntry={true}
        type="password"
        component={myFields}
        label="Password"
      />

      <TouchableOpacity
        style={styles.loginButton}
        onPress={handleSubmit (submit)}
      >
        <Text style={styles.buttonText}>Sign In</Text>
      </TouchableOpacity>
    </View>
  );
};

export default reduxForm ({
  // a unique name for the form
  form: 'Login',
  validate,
}) (LoginForm);
