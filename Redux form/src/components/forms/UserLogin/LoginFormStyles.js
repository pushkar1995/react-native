import {StyleSheet} from 'react-native';

export default StyleSheet.create ({
  loginButton: {
    width: 350,
    height: 50,
    borderWidth: 0.5,
    borderColor: '#078AC0',
    borderRadius: 100,
    justifyContent: 'center',
    marginTop: 55,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '100',
    color: '#078AC0',
    textAlign: 'center',
  },
  inputBox: {
    width: 350,
    height: 60,
    borderBottomWidth: 1,
    borderColor: '#078AC0',
    fontSize: 16,
    fontWeight: '100',
    textAlign: 'right',
  },
  inputField: {
    padding: 10,
  },
});
