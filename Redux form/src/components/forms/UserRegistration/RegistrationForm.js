import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';

import styles from './RegistrationFormStyles';

//define validation
const validate = values => {
  const errors = {};

  if (!values.fullName) {
    errors.fullName = 'Name is required';
  }

  if (!values.phoneNo) {
    errors.phoneNo = 'Phone is required';
  } else if (values.phoneNo.length != 10) {
    errors.phoneNo = 'Must be 10 digits';
  }

  if (!values.email) {
    errors.email = 'Email is required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test (values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.password) {
    errors.password = 'Password is required';
  } else if (values.password.length < 8) {
    errors.password = 'Password must be minimum 8 characters';
  } else if (/[^a-zA-Z0-9 ]/i.test (values.password)) {
    errors.password = 'password must be alphanumeric';
  }

  if (!values.confirmPassword) {
    errors.confirmPassword = 'Confirm Password is required';
  } else if (values.password.length < 8) {
    errors.password = 'Password must be minimum 8 characters';
  } else if (values.confirmPassword !== values.password) {
    errors.confirmPassword = 'Password mismatched';
  }

  return errors;
};

const submit = values => {
  alert (values);
};

// render your fields
const myFields = ({
  label,
  secureTextEntry,
  type,
  meta: {error, touched},
  input: {onChange},
}) => {
  return (
    <View>
      <TextInput
        style={styles.inputBox}
        placeholder={label}
        type={type}
        secureTextEntry={secureTextEntry}
        onChangeText={onChange}
        placeholderTextColor="#ffff"
        underlineColorAndroid="transparent"
      />
      {touched && (error && <Text style={{color: 'red'}}>{error}</Text>)}
    </View>
  );
};

const RegistrationForm = props => {
  const {handleSubmit} = props;
  return (
    <View>
      <Field
        style={styles.inputField}
        name="fullName"
        component={myFields}
        label="Name"
      />

      <Field
        style={styles.inputField}
        name="phoneNo"
        component={myFields}
        label="Phone"
      />

      <Field
        style={styles.inputField}
        name="email"
        component={myFields}
        label="E-mail"
        type="email"
      />

      <Field
        style={styles.inputField}
        name="password"
        secureTextEntry={true}
        component={myFields}
        label="Password"
      />

      <Field
        style={styles.inputField}
        name="confirmPassword"
        secureTextEntry={true}
        component={myFields}
        label="Confirm Password"
      />

      <TouchableOpacity
        style={styles.submitButton}
        onPress={handleSubmit (submit)}
      >
        <Text style={styles.buttonText}>Submit</Text>
      </TouchableOpacity>
    </View>
  );
};

export default reduxForm ({
  // a unique name for the form
  form: 'Signup',
  validate,
}) (RegistrationForm);
