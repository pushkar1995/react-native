import {StyleSheet} from 'react-native';

export default StyleSheet.create ({
  submitButton: {
    justifyContent: 'center',
    width: 100,
    height: 40,
    borderColor: '#02A2DF',
    borderWidth: 0.5,
    borderRadius: 100,
    marginTop: 50,
    marginLeft: 250,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '100',
    color: '#078AC0',
    textAlign: 'center',
  },
  inputBox: {
    width: 350,
    height: 60,
    borderBottomWidth: 1,
    borderColor: '#078AC0',
    fontSize: 16,
    fontWeight: '100',
    textAlign: 'right',
  },
  inputField: {},
});
