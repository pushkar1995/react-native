import React, {Component} from 'react';
import {Text, View, ScrollView} from 'react-native';

import styles from './RegistrationPageStyles';

import RegistrationForm from './RegistrationForm';

export default class RegistrationPage extends Component {
  render () {
    return (
      <ScrollView style={styles.registerPageContainer}>
        <Text style={styles.titleText}>Sign up</Text>

        <View style={styles.form}>
          <RegistrationForm />
        </View>
      </ScrollView>
    );
  }
}
