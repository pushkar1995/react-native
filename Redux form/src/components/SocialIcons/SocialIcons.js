import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

import FacebookIcon from 'react-native-vector-icons/EvilIcons';
import GoogleIcon from 'react-native-vector-icons/FontAwesome';

export default class Home extends Component {
  render () {
    return (
      <View style={styles.container}>

        <View style={styles.buttons}>
          <TouchableOpacity style={styles.googleButton}>
            <View style={styles.googleIcon}>
              <GoogleIcon name="google" size={30} color="#ffff" />
            </View>
            <Text style={styles.googleLoginText}>Login with Google +</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.facebookButton}>
            <View style={styles.facebookIconBackground}>
              <FacebookIcon name="sc-facebook" size={30} color="#ffff" />
            </View>
            <Text style={styles.facebookLoginText}>Login with Facebook</Text>

          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    alignSelf: 'center',
    padding: 10,
  },
  buttons: {
    height: 150,
    justifyContent: 'space-around',
  },
  facebookButton: {
    flexDirection: 'row',
    height: 50,
    width: 350,
    borderRadius: 100,
    backgroundColor: '#4267B2',
    alignItems: 'center',
  },
  facebookIconBackground: {
    height: 35,
    width: 35,
    borderRadius: 100,
    backgroundColor: '#6081C4',
    marginHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  facebookLoginText: {
    flex: 2,
    fontSize: 14,
    fontWeight: '100',
    marginHorizontal: 50,
    color: '#ffff',
  },
  googleButton: {
    flexDirection: 'row',
    height: 50,
    width: 350,
    borderRadius: 100,
    backgroundColor: '#EB4335',
    alignItems: 'center',
  },
  googleIcon: {
    marginHorizontal: 15,
  },
  googleLoginText: {
    flex: 2,
    fontSize: 14,
    fontWeight: '100',
    marginHorizontal: 50,
    color: '#ffff',
  },
});
