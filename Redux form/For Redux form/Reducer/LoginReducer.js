import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions ({
  loginSubmitRequest: ['userdetails'],
  loginSubmitSuccess: ['payload'],
  loginSubmitFailure: null,
});

export const loginSubmitTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable ({
  userdetails: null,
  fetching: true,
  payload: null,
  error: false,
});

/* ------------- Reducers ------------- */

// request the data from an api
export const performSubmitRequest = (state, {userdetails}) => {
  console.tron.log ('submit workd');
  return state.merge ({fetching: true, userdetails, payload: null});
};

// successful api lookup
export const performSubmitSuccess = (state, action) => {
  const {payload} = action;
  return state.merge ({fetching: false, error: null, payload});
};

// Something went wrong somewhere.
export const performSubmitFailure = state => {
  state.merge ({fetching: false, error: true, payload: null});
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer (INITIAL_STATE, {
  [Types.LOGIN_SUBMIT_REQUEST]: performSubmitRequest,
  [Types.LOGIN_SUBMIT_SUCCESS]: performSubmitSuccess,
  [Types.LOGIN_SUBMIT_FAILURE]: performSubmitFailure,
});
