import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import styles from './SlidingContentStyles';

import PlayIcon from 'react-native-vector-icons/MaterialIcons';
import StarIcon from 'react-native-vector-icons/MaterialIcons';

export default class SliderContent extends Component {
  render () {
    return (
      <View style={styles.container}>

        <View style={styles.LeftImage}>
          <Image
            source={{
              uri: 'https://i0.wp.com/xnepali.net/wp-content/uploads/2017/09/aishwarya-nepali-movie-poster-ramesh-upreti.jpg?w=665&ssl=1',
            }}
            style={styles.Image}
          />
        </View>

        <View style={styles.MaterialPlayIcon}>
          <PlayIcon name="play-circle-outline" size={65} color="white" />
        </View>

        <View style={styles.RightContent}>

          <View style={styles.RightTopContent}>
            <Text style={{fontSize: 20, color: '#ffffff'}}>
              Name of{' '}
            </Text>
            <Text style={{fontSize: 23, fontWeight: 'bold', color: '#ffffff'}}>
              Documentary{' '}
            </Text>

          </View>

          <View style={styles.TopBottomBorder} />

          <View style={styles.RightBottomContent}>
            <Text style={{color: '#D3D3D3', fontSize: 20}}>Actor Name </Text>
          </View>

          <View style={styles.MaterialStarIcon}>
            <StarIcon name="star" size={15} color="yellow" />
            <StarIcon name="star" size={15} color="yellow" />
            <StarIcon name="star" size={15} color="yellow" />
            <StarIcon name="star" size={15} color="yellow" />
            <StarIcon name="star" size={15} color="yellow" />
          </View>

        </View>

      </View>
    );
  }
}

