import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 5,
      },
    
      LeftImage: {
        flex: 2,
        borderRadius: 5,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 5,
        borderTopRightRadius: 0,
        height: 168,
        backgroundColor: '#141529',
      },
    
      Image: {
        height: 167,
        borderRadius: 5,
      },
    
      MaterialPlayIcon: {
        position: 'absolute',
        marginTop: 55,
        marginHorizontal: 55,
      },
    
      MaterialStarIcon: {
        flexDirection: 'row',
        position: 'absolute',
        marginTop: 130,
        marginHorizontal: 10,
      },
    
      RightContent: {
        flex: 3,
        backgroundColor: '#141529',
        borderRadius: 5,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
      },
    
      RightTopContent: {
        paddingTop: 10,
        paddingLeft: 10,
        height: 90,
      },
    
      TopBottomBorder: {
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: '#D3D3D3',
        marginHorizontal: 10,
      },
    
      RightBottomContent: {
        paddingTop: 10,
        paddingLeft: 10,
        alignSelf: 'stretch',
        height: 70,
        borderBottomRightRadius: 10,
      },

})