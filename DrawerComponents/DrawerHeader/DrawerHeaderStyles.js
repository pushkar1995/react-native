import {StyleSheet} from 'react-native';

export default StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#1B1D2B',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  logoStyle: {
    width: 200,
    height: 150,
  },
  profileName: {
    fontSize: 16,
    color: '#ffff',
    fontWeight: 'bold',
  },
});
