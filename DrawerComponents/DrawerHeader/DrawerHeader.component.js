import React, {Component} from 'react';
import {View, TouchableHighlight, Image, Text} from 'react-native';

//Styles
import styles from './DrawerHeaderStyles';

export default class ProfilePage extends Component {
  // constructor(props) {
  // super(props);

  // this.state = { uri: 'https://cdn.dribbble.com/users/124355/screenshots/2199042/profile_1x.png' }
  // }

  // changeLogo() {
  //     console.log('state changed!');
  //     this.setState({
  //     uri: 'https://cdn.dribbble.com/users/124355/screenshots/2199042/profile_1x.png'
  //     });
  // }

  render () {
    return (
      <View style={styles.container}>
        <TouchableHighlight
        //onPress={() => this.changeLogo()}
        >
          <Image
            source={{
              uri: 'https://cdn.dribbble.com/users/124355/screenshots/2199042/profile_1x.png',
            }}
            style={styles.logoStyle}
          />
        </TouchableHighlight>

        <Text style={styles.profileName}>User Name</Text>

      </View>
    );
  }
}
