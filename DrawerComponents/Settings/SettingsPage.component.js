import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';

export default class SettingsPage extends Component {
  render () {
    return (
      <View style={styles.container}>
        <Text>Settings Page</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#1C1E33',
  },
});
