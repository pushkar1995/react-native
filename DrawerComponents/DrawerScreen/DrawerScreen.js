import React, {Component} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

//Styles
import styles from './DrawerScreenStyles';

import DrawerHeader from '../DrawerHeader/DrawerHeader.component';
import NotificationIcon from 'react-native-vector-icons/Ionicons';
import SettingsIcon from 'react-native-vector-icons/Ionicons';
import ProfileIcon from 'react-native-vector-icons/Ionicons';

export default class DrawerScreen extends Component {
  render () {
    return (
      <View style={styles.container}>

        <View style={styles.headerContainer}>
          <DrawerHeader />
        </View>

        <View style={styles.bodyContainer}>

          {/* <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate ('Notifications')}
          >
            <Text style={styles.buttonText}>Notifications</Text>
            <NotificationIcon
              style={styles.icons}
              name="md-notifications"
              size={20}
            />
         </TouchableOpacity> 

          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate ('Profile')}
          >
            <Text style={styles.buttonText}>Profile Page</Text>
            <ProfileIcon style={styles.icons} name="md-contact" size={20} />
          </TouchableOpacity> */}

          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate ('UserLogin')}
          >
            <Text style={styles.buttonText}>Login</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate ('UserRegistration')}
          >
            <Text style={styles.buttonText}>Sign up</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate ('Settings')}
          >
            <Text style={styles.buttonText}>Settings</Text>
            <SettingsIcon style={styles.icons} name="md-settings" size={20} />
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate ('Contactus')}
          >
            <Text style={styles.buttonText}>Contact us</Text>
          </TouchableOpacity>

          {/* <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate ('UserLogin')}
          >
            <Text style={styles.buttonText}>Logout</Text>
          </TouchableOpacity> */}

        </View>
      </View>
    );
  }
}
