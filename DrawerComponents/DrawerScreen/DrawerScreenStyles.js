import {StyleSheet} from 'react-native';

export default StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  headerContainer: {
    flex: 2,
    flexDirection: 'row',
  },
  bodyContainer: {
    flex: 4,
    flexDirection: 'column',
    backgroundColor: '#1C1E33',
  },
  button: {
    borderWidth: 0.5,
    color: '#131528',
    padding: 15,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '300',
    color: '#ffff',
    paddingLeft: 30,
  },
  icons: {
    position: 'absolute',
    marginLeft: 15,
    marginTop: 20,
    color: 'grey',
  },
});
