import React, {Component} from 'react';
import { Text, View} from 'react-native';

import SearchBox from '../SearchBox/SearchBox.component'

//Styles
import styles from './VideoListingStyles'

export default class VideoListingScreen extends Component {
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.title}>
            <Text style ={styles.titleText}>Featured Movies</Text>
        </View>
        <SearchBox />
      </View>
    );
  }
}


