import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1C1D34',
      },
      title: {
        marginHorizontal: 30,
        marginTop: 5,
      },
      titleText: {
        fontSize:20, 
        color:'#ffff', 
        fontWeight: 'bold'
      }

})