import {StyleSheet, Dimensions} from 'react-native';


export default StyleSheet.create ({
    container: {
        flexDirection: 'column',
        backgroundColor: '#1C1E33',
        padding: 10,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: Dimensions.get('window').height/4
      },
        crossIcon: {
          alignSelf: 'flex-end',
          color: '#ffff',
      },
      title: {
        color: '#ffff',
        fontSize: 30,
        fontWeight: '100',
      },
      onlyText: {
        color: '#ffff',
        fontSize: 10,
        fontWeight: '100',
      },
      cashTitle: {
        color: '#037FF3',
        fontSize: 40,
      },
      daysText: {
        color: '#037FF3',
        fontSize: 20,
      },
      body: {
        padding:10,
      },
      applePaymentButton: {
        width: 350,
        height: 50,
        backgroundColor: '#000000',
        borderRadius: 100,
        justifyContent: 'center',
      },
      applePayButtonText: {
        fontSize: 16,
        fontWeight: '100',
        color: '#ffff',
        marginLeft: 120,
      },
      payText: {
        fontSize: 16,
        fontWeight: '100',
        color: '#ffff',
        marginLeft: 210,
        marginTop: -22,
      },
      appleIcons: {
        position: 'absolute',
        marginLeft: 190,
        color: '#ffff',
      },
      eSewaPaymentButton: {
        width: 350,
        height: 50,
        backgroundColor: '#5FB946',
        borderRadius: 100,
        justifyContent: 'center',
        marginTop: 15,
      },
      eSewaButtonText: {
        fontSize: 16,
        fontWeight: '100',
        color: '#ffff',
        textAlign: 'center',
      },
      eSewaImage: {
        position: 'absolute',
        height: 30,
        width: 30,
        marginLeft: 30,
        marginBottom: 20,
    }

});