import React, {Component} from 'react';
import {StyleSheet, View, Text, Button, TouchableOpacity, Dimensions, Modal} from 'react-native';

import AppleIcon from 'react-native-vector-icons/Ionicons';
import AddIcon from 'react-native-vector-icons/Ionicons';
import CrossIcon from 'react-native-vector-icons/Entypo'

import styles from './RentMovieDialogStyles';

export default class RentMDialogScreen extends Component {
  state={
    showModal:false
  }
  render () {
    return (
      <View>
      <Modal visible={this.state.showModal} transparent={true} animationType="fade">
      <View style={styles.container}>

        <View style={{justifyContent:'flex-end', alignSelf:'flex-end'}}>
        <TouchableOpacity onPress={()=>{
          this.setState({
            showModal: false
          });
        }}>
        <CrossIcon name="circle-with-cross" style={styles.crossIcon} size={20} />
        </TouchableOpacity>
        </View>

        <View style={styles.header}>
          <Text style={styles.title}>Rent this movie</Text>
          <Text style={styles.onlyText}>For only,</Text>
          <Text style={styles.cashTitle}>
            $2.99/<Text style={styles.daysText}>3 days</Text>
          </Text>
        </View>

        <View style={styles.body}>

          <TouchableOpacity
            style={styles.applePaymentButton}
            onPress={this._Login}
          >
            <Text style={styles.applePayButtonText}>
              Pay with
            </Text>
            <Text style={styles.payText}>Pay</Text>
            <AppleIcon name="logo-apple" style={styles.appleIcons} size={20} />
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.eSewaPaymentButton}
            onPress={this._Login}
          >
            <Text style={styles.eSewaButtonText}>
              Pay with eSewa
            </Text>
          </TouchableOpacity>

        </View>

      </View>
      </Modal>
      <View>
        
      </View>


      <Button title="Show Modal" onPress={()=> this.setState({showModal: true})} />
      </View>
    );
  }
}

